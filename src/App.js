import React, { useState, useEffect} from 'react' 
import { firebase } from './firebase'

const App = () => {
  const [tareas,setTareas] = useState([])
  const [tarea , setTarea] = useState('')
  const [modoEdicion, setModoEdicion] = useState(false)
  const [id , setId] = useState('')
  const [change, setChange] = useState(false)

  useEffect(() => {
    const getData = async () =>{

      try {
        const db = firebase.firestore();
        const data = await db.collection('tareas').get();
        const arrayData = data.docs.map(doc => ({id: doc.id, ...doc.data()}));
        setTareas(arrayData);

      } catch (error) {
        console.log(error)
      }
    }
    getData()
  }, [])

  const addTask = e => {
    setTarea(e.target.value);
    setChange(true)
  }

  const add = async e =>{
    e.preventDefault()
    if (!tarea.trim()) {
      console.log('esta vacio')
      return
    }
    try {
      const db = firebase.firestore()
      const newTask = {
        name: tarea,
        fecha: Date.now()
      }
      const data = await db.collection('tareas').add(newTask)

      setTareas([
        ...tareas,
        {...newTask, id: data.id}
      ])

      setTarea('')
      setChange(false)
    } catch (error) {
      console.log(error)
    }
  }

  const deleteTask = async id => {
    try {
      const db = firebase.firestore()
      await db.collection('tareas').doc(id).delete()
      const arrayFiltrado = tareas.filter(tarea => tarea.id !== id);
      setTareas(arrayFiltrado);
    } catch (error) {
      console.log(error)
    }
  }

  const activateEdit = (tarea) =>{
    setModoEdicion(true)
    setTarea(tarea.name)
    setId(tarea.id)
    setChange(true)
  }

  const edit = async e => {
    e.preventDefault()
    if (!tarea.trim()) {
      console.log('esta vacio')
      return
    }
    try {
      const db = firebase.firestore()
      await db.collection('tareas').doc(id).update({
        name : tarea
      })
    } catch (error) {
      console.log(error)
    }
    const arrayEdited = tareas.map(task =>  
        (task.id === id ? {id: task.id, date: task.date, name: tarea} : task
      ))
    setTareas(arrayEdited)
    setModoEdicion(false)
    setTarea('')
    setId('')
    setChange(false)
  }

  const cancel = () =>{
    setTarea('')
    setChange(false)
    setModoEdicion(false)
  }

  return (
    <div className='container mt-3'>
      <div className='row'>
        <div className='col-6'>
          <h3>Lista de Tareas</h3>
          <ul className='list-group'>
            {
              tareas.map(tarea =>(
                <li key={tarea.id} className='list-group-item'> 
                  { tarea.name }
                  <button 
                  className='btn btn-danger btn-sm float-right'
                  onClick={()=> deleteTask(tarea.id)}
                  > 
                    Eliminar 
                  </button>
                  <button className='btn btn-warning btn-sm float-right mr-2'
                  onClick={() => activateEdit(tarea)}
                  > 
                    Editar 
                  </button>
                </li>
              ))
            }
          </ul>
        </div>
        <div className='col-6'>
          <h3>{modoEdicion ? 'Editar Tarea ' : 'Agregar Tarea'}</h3>
          <form onSubmit={modoEdicion? edit : add}>
            <input 
              type='text'
              placeholder='Ingrese una tarea'
              className='form-control mb-2'
              onChange= {addTask}
              value={tarea}
            >

            </input>
            <button className= { modoEdicion ? 'btn btn-warning btn-block' : 'btn btn-dark btn-block' } 
            type='submit'
            > 
              { modoEdicion ? 'Editar' : 'Agregar'} 
            </button>
            {
              change ? 
              <button 
                className='btn btn-outline-secondary btn-block' 
                onClick={ () => cancel() }
              >
                Cancelar 
              </button>
              : null
            }
          </form>
        </div>
      </div>
    </div>
  )
}

export default App

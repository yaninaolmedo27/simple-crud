import firebase from 'firebase/app';
import 'firebase/firestore'

const firebaseConfig = {
  apiKey: "AIzaSyAXYhWAZGgi2LBIPHx6Zui2hogZvc5hkFQ",
  authDomain: "crud-firestore-react.firebaseapp.com",
  databaseURL: "https://crud-firestore-react.firebaseio.com",
  projectId: "crud-firestore-react",
  storageBucket: "crud-firestore-react.appspot.com",
  messagingSenderId: "818850341273",
  appId: "1:818850341273:web:64663b48c8ba9f01b33308",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export {firebase}